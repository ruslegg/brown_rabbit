function changePage(number){
	var posts = document.querySelectorAll('.news-post');
	for (var i = posts.length - 1; i >= 0; i--) {
		if (!posts[i].classList.contains('no-show-post')) {
			posts[i].classList.add('no-show-post');
		}

	}
	for (var i = number*3; i >= number*3-2; i--) {
		document.getElementById('post'+i).classList.remove('no-show-post');
	}
	document.getElementById("page-number").innerHTML = "Page "+number+" of 3";

}
var imageIndex = 0;
slideShow();
function slideShow() {
    var i;
    var images = document.getElementsByClassName("image-slider");
    for (i = 0; i < images.length; i++) {
        images[i].style.display = "none"; 
    }
    imageIndex++;
    if (imageIndex > images.length) {imageIndex = 1} 
    images[imageIndex-1].style.display = "block"; 
    setTimeout(slideShow, 6000);
}